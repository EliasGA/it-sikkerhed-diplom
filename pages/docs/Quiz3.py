print("\nVelkommen til InfoSec Quiz. Besvar alle 5 rigtige for at se koden\n")
input ("Tryk på enter for at starte\n")

question = 0    # Fortæller hvilket spørgsmål brugeren har gennemført
questions = ["Hvilke tre grundsten består informationssikkerhed af?: ",
            "Hvor må informationer klassificeret som 'Til arbejdsbrug' opbevares?: ",
            "Du har modtaget en mistænkelig mail fra en kollega som beder dig trykke på et link hastigt - hvad gør du?: ",
            "Må personer med rødt bånd befinde sig i Kuben? ",
            "Du skal sende et meget fortroligt dokument til en kollega via mail. Hvad er den sikreste måde? "]

answers = ["\na. Fortrolighed, Integritet, Tilgængelighed \nb. Autencitet, Integritet, Tilgængelighed \nc. Fortrolighed, Pålidelighed, Ansvarlighed \nd. Hemmelig, uafviselighed, Tilgængelighed\n",
       "\na. Energinet-enheder\nb. Private enheder\nc. Dropbox/Google Drive\nd. Alle, så længe det ikke er fortroligt\n",
       "\na. Ringer til kollegaen for at verificere mailen\nb. Trykker på linket, da det er fra en kollega\nc. Besvar mailen for at dobbelttjekke om linket er sikkert\nd. Ignorer mailen\n",
       "\na. Nej\nb. Ja\nc. Ja, hvis de er sammen med et sort bånd\nd. Måske\n",
       "\na. Krypter beskeden med Outlook Encrypt\nb. Du kan frit sende når det er internt\nc. Send dokomentet via Dropbox\nd. Det må ikke sendes på mail\n"]

correct_answers = {0:'a',
               1:'a',
               2:'a',
               3:'a',
               4:'a'}

correct = 0 #variabel til at gemme score

while question < 5:
    print(questions[question])
    print(answers[question])
    answer = input("Svar med a, b, c eller d\n").lower()

    while answer != correct_answers[question]:
        print("\nIkke korrekt")
        answer = input("Prøv igen\n").lower()

    print("\nKorrekt\n")
    correct += 1
    question += 1

    if question == 5 and correct == 5:
        print("Tillykke! Alle spørgsmål blev besvaret rigtigt. Koden er: 1234\n")

            