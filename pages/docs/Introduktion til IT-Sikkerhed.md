## **Netværksanalyse**

Nmap: Netværksscanning tool

Wireshark: Netværksanalyse tool

[Nmap](https://tryhackme.com/room/furthernmap)

[Wireshark](https://tryhackme.com/room/wiresharkthebasics)

## **Opsætning af vsrx router**
## **softwaresikkerhed python**

Opgave: at indsætte en kommentar efter hver linje kode der definere hvad den gør
    
    #Definere en funktion der tager to parametre og returere deres sum
    def add(num1, num2):
        return num1 + num2

    def sub(num1, num2):
        return num1 - num2

    def div(num1, num2):
        return num1 / num2

    def mult(num1, num2):
        return num1 * num2
    
    #Printer velkomsbesked til brugeren
    print('\nWelcome to the simple calculator (write q to quit)')
   
    #variabel for resultatet af handlingen og den valgte handling 
    result = 0.0
    chosen_operation = ''

    #Starter et uendeligt loop der beder brugeren om at indtaste to tal og en handling (add, sub, div, mult) indtil de vælger at afslutte med q.
    while True:
        print('enter the first number')
        number1 = input('> ')
        if number1 == 'q':
            print('goodbye...')
            break
        print('enter the second number')
        number2 = input('> ')
        if number2 == 'q':
            print('goodbye...')
            break
        print('would like to: 1. add 2. subtract 3. divide or 4. multiply the numbers?')
        operation = input('> ')
        if operation == 'q':
            print('goodbye...')
            break

        #Konverter den inputede string til float og integer
        try:
            number1 = float(number1)
            number2 = float(number2)
            operation = int(operation)

            #Kalder på den definerede funktion til at lave udregningen baseret på brugeren valg
            #Definere 'chosen_operation' til en string der bruges til at printe beskeden til brugeren
            if operation == 1:
                result = add(number1, number2)
                chosen_operation = ' added with '
            elif operation == 2:
                result = sub(number1, number2)
                chosen_operation = ' subtracted from '
            elif operation == 3:
                result = div(number1, number2)
                chosen_operation = ' divided with '
            elif operation == 4:
                result = mult(number1, number2)
                chosen_operation = ' multiplied with '

            #Printer resultatet af udregningen og genstarter loopet 
            print(str(number1) + chosen_operation + str(number2) + ' = ' + str(result))
            print('restarting....')

        "Hvis konverteringen til float/integer fejler, så printes en fejlbesked og loopet genstartes
        except ValueError:
            print('only numbers and "q" is accepted as input, please try again')

        #Hvis brugeren prøver at div med 0, så printes en fejlbesked og loopet genstartes 
        except ZeroDivisionError:
            print('cannot divide by zero, please try again')

Opgave 2: Omskriv koden så der er færre gentagelser
 
    # Definere en funktion der tager tre parametre (2 tal og valgte handling) og returnere resultatet og valgte handling som en tuple
    def operate(num1, num2, operation):
        if operation == 1:
            return num1 + num2, ' added with '
        elif operation == 2:
            return num1 - num2, ' subtracted from '
        elif operation == 3:
            return num1 / num2, ' divided with '
        elif operation == 4:
            return num1 * num2, ' multiplied with '
        else:
            return None, ''

    # Printer velkomsbesked
    print('\nWelcome to the simple calculator (write q to quit)')

    # Initiere variabler
    result = 0.0
    chosen_operation = ''

    # Stater et uendelig loop der prompter brugeren for svar
    while True:
        print('enter the first number')
        number1 = input('> ')
        if number1 == 'q':
            print('goodbye...')
            break

        print('enter the second number')
        number2 = input('> ')
        if number2 == 'q':
            print('goodbye...')
            break

        print('would like to: 1. add 2. subtract 3. divide or 4. multiply the numbers?')
        operation = input('> ')
        if operation == 'q':
            print('goodbye...')
            break

        try:
            number1 = float(number1)
            number2 = float(number2)
            operation = int(operation)

            # Kalder på funktionen til at udføre handlingen
            result, chosen_operation = operate(number1, number2, operation)

            # Printer resultatet og genstarter loopet
            print(str(number1) + chosen_operation + str(number2) + ' = ' + str(result))
            print('restarting....')

        except ValueError:
            print('only numbers and "q" is accepted as input, please try again')

        except ZeroDivisionError:
            print('cannot divide by zero, please try again')
        