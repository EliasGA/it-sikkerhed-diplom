# IT-Sikkerhed Diplom

Dette er en test

For full documentation visit [mkdocs.org](https://www.mkdocs.org).

## Commands

* `mkdocs new [dir-name]` - Create a new project.
* `mkdocs serve` - Start the live-reloading docs server.
* `mkdocs build` - Build the documentation site.
* `mkdocs -h` - Print help message and exit.

git add og commit nye filer

* Tilføj nye filer med `git add .`

* Commit med `git commit -am "added mkdocs"`

* Push med `git push`

## Markdown

[Markdown Cheatsheet](https://www.markdownguide.org/cheat-sheet)

## Project layout

    mkdocs.yml    # The configuration file.
    docs/
        index.md  # The documentation homepage.
        ...       # Other markdown pages, images and other files.
